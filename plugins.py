#!/usr/bin/env python

from binaryninja.pluginmanager import RepositoryManager
from binaryninja.interaction import show_message_box, AddressField, ChoiceField, LabelField, get_form_input, SeparatorField
from binaryninja.plugin import PluginCommand

mgr = RepositoryManager()

def doit(probably_a_view):
    inputs = []
    actions = {}
    inputs.append(LabelField("Welcome to the plugin manager"))
    for plugin in mgr.plugins['default']:
        inputs.append(SeparatorField())
        inputs.append(LabelField(plugin.name))
        inputs.append(LabelField(plugin.description))
        if plugin.installed and plugin.enabled:
            choices = ["Installed - Enabled", "Installed - Disabled", "Uninstalled - Disabled"]
        elif plugin.installed:
            choices = ["Installed - Disabled", "Installed - Enabled", "Uninstalled - Disabled"]
        else:
            choices = ["Uninstalled - Disabled", "Installed - Enabled", "Installed - Disabled"]
        actions[plugin.name] = ChoiceField("Status?", choices)
        inputs.append(actions[plugin.name])

    get_form_input(inputs, "Plugin Manager")
    for plugin in mgr.plugins['default']:
        if actions[plugin.name].choices[actions[plugin.name].result] == "Installed - Enabled":
            mgr.install_plugin(plugin.path)
            mgr.enable_plugin(plugin.path)
        elif actions[plugin.name].choices[actions[plugin.name].result] == "Installed - Disabled":
            mgr.install_plugin(plugin.path)
            mgr.disable_plugin(plugin.path)
        else:
            if plugin.enabled:
                mgr.disable_plugin(plugin.path)
            if plugin.installed:
                mgr.uninstall_plugin(plugin.path)

PluginCommand.register("Plugin Manager", "Plugin Manager", doit)